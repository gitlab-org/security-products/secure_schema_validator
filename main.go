package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"text/template"

	"github.com/xeipuuv/gojsonschema"
)

const (
	defaultType    = "sast"
	defaultVersion = "v14.1.3"
)

var (
	localSchema = ""
	templates   = template.Must(template.ParseFiles("index.html"))
	schemaTypes = []string{
		"container-scanning",
		"coverage-fuzzing",
		"dast",
		"dependency-scanning",
		"sast",
		"secret-detection",
	}
	schemaVersions = []string{
		defaultVersion,
		"v15.0.4",
		"v14.1.0",
		"v14.0.0",
		"v13.1.0",
		"v3.0.0",
	}
)

type Document struct {
	Body             string
	Errors           []gojsonschema.ResultError
	SchemaTypes      []string
	ValidatorType    string
	SchemaVersions   []string
	ValidatorVersion string
}

func schemaURI(schemaType string, version string) string {
	return "https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/" + version + "/dist/" + schemaType + "-report-format.json"
}

func validate(schema string, body string) (string, []gojsonschema.ResultError) {
	result, err := gojsonschema.Validate(
		gojsonschema.NewReferenceLoader(schema),
		gojsonschema.NewStringLoader(body),
	)
	if err != nil {
		return err.Error(), []gojsonschema.ResultError{}
	}

	return "", result.Errors()
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	doc := &Document{
		SchemaTypes:      schemaTypes,
		ValidatorType:    r.FormValue("schema_type"),
		SchemaVersions:   schemaVersions,
		ValidatorVersion: r.FormValue("schema_version"),
		Body:             r.FormValue("body"),
	}

	// Set defaults onLoad
	if r.Method == "GET" {
		doc.ValidatorType = defaultType
		doc.ValidatorVersion = defaultVersion
	}

	// Validate schema type and version
	if !contains(doc.ValidatorType, schemaTypes) || !contains(doc.ValidatorVersion, schemaVersions) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid schema_type and/or schema_version"))
		return
	}

	schema := localSchema
	if doc.ValidatorType != defaultType || doc.ValidatorVersion != defaultVersion {
		schema = schemaURI(doc.ValidatorType, doc.ValidatorVersion)
	}

	if len(doc.Body) > 1 {
		err, results := validate(schema, doc.Body)
		if err != "" {
			http.Error(w, err, http.StatusInternalServerError)
			return
		}

		if len(results) >= 0 {
			doc.Errors = results
		}
	}

	err := templates.ExecuteTemplate(w, "index.html", doc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func contains(s string, in []string) bool {
	for _, el := range in {
		if el == s {
			return true
		}
	}

	return false
}

// prefetch schema version to reduce network requests
func init() {
	fmt.Printf("Fetching schema for local caching: %s... ", schemaURI("sast", defaultVersion))

	resp, err := http.Get(schemaURI("sast", defaultVersion))
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("done")

	tmpFile, err := ioutil.TempFile(os.TempDir(), defaultVersion)
	if err != nil {
		panic(err)
	}
	defer tmpFile.Close()

	_, err = io.Copy(tmpFile, resp.Body)
	if err != nil {
		panic(err)
	}

	localSchema = "file://" + tmpFile.Name()
}

func main() {
	http.HandleFunc("/", rootHandler)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
